﻿#include <iostream> 
#include <ctime>
#include <cmath>
#include <windows.h>

using namespace std;

void pirfy(float arr[], float n)
{
	int i = 0;
	int large = i;
	int l = 2 * i + 1;
	int r = 2 * i + 2;
	if (l < n && arr[l] > arr[large])
	{
		large = l;
	}


	if (r < n && arr[r] > arr[large])
	{
		large = r;
	}

	if (large != i)
	{
		swap(arr[i], arr[large]);

		pirfy(arr, n);
	}
}

void pir(float arr[], float n)
{

	for (int i = n / 2 - 1; i >= 0; i--)
	{
		pirfy(arr, n);
	}

	for (int i = n - 1; i >= 0; i--)
	{

		swap(arr[0], arr[i]);


		pirfy(arr, i);
	}
}

void SortShell(float x[], float n)   //відбуваться сортування Шелла
{
	int i, j, k, a, tmp;
	float swap = 0, comp = 0; a = n / 2;
	while (a > 0)
	{
		for (i = 0; i < a; i++)
		{
			for (j = 0; j < n; j += a)
			{
				tmp = x[j];
				for (k = j - a; k >= 0 && tmp < x[k]; k -= a)
				{
					comp++;
					swap++;
					x[k + a] = x[k];
				}
				x[k + a] = tmp;
				swap++;
			}
		}
		comp++;
		if
			(a / 2 != 0)
			a = a / 2;
		else if
			(a == 1)
			a = 0;
		else
			a = 1;
	}
}

void Sort(float x[], float n)
{
	int i, j, k, a, tmp;
	float swap = 0, comp = 0;
	float S;
	S = (float)log(n + 1) / log(2);
	a = (float)pow(2, S) - 1;
	while (a > 0)
	{
		for (i = 0; i < a; i++)
		{
			for (j = 0; j < n; j += a)
			{
				tmp = x[j];
				for (k = j - a; k >= 0 && tmp < x[k]; k -= a)
				{
					comp++;
					swap++;
					x[k + a] = x[k];
				}
				x[k + a] = tmp;
				swap++;
			}
		}
		comp++;
		S--;
		if (a != 1)
		{
			a = pow(2, S) - 1;
		}
		else
		{
			a = 0;
		}
	}
}

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);


	for (int u = 0; u < 3; u++) {
		if (u == 1)
		{
			float n = 10000;
			float* arr = new float[n];
			float* array = new float[n];
			for (int i = 0; i < n; i++)
			{
				arr[i] = 10 + rand() % 91;
				array[i] = arr[i];
			}
			double time1 = clock() / 1000.0;
			pir(arr, n);
			double time2 = clock() / 1000.0;
			printf("\nЧас : %f секунд", time2 - time1);
			delete[] arr;
			printf("\n");
		}
		if (u == 2)
		{
			float n = 10000;
			float* arr = new float[n];
			float* array = new float[n];
			for (int i = 0; i < n; i++)
			{
				arr[i] = rand() % 261 - 10;
				array[i] = arr[i];
			}

			double time1 = clock() / 1000.0;
			SortShell(arr, n);
			  Sort(array, n);
			  double time2 = clock() / 1000.0;
			  printf("\nЧас : %f секунд", time2 - time1);
			  delete[] arr;
			  printf("\n");
		}
		
	}

	




}
