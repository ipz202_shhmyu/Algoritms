﻿using System;
using System.Text;
using System.Diagnostics;

namespace sorter
{
    class Program
    {
        public static void Show<N>(N[] arr)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                Console.Write(arr[i]);
            }
            Console.WriteLine();
        }
       
        static char[] countingSort(char[] array)
        {
            char[] sortedArray = new char[array.Length];
            char minVal = array[0];
            char maxVal = array[0];
            for (int i = 1; i < array.Length; i++)
            {
                if (array[i] < minVal) minVal = array[i];
                else if (array[i] > maxVal) maxVal = array[i];
            }
            int[] counts = new int[maxVal - minVal + 1];
            for (int i = 0; i < array.Length; i++)
            {
                counts[array[i] - minVal]++;
            }
            counts[0]--;
            for (int i = 1; i < counts.Length; i++)
            {
                counts[i] = counts[i] + counts[i - 1];
            }
            for (int i = array.Length - 1; i >= 0; i--)
            {
                sortedArray[counts[array[i] - minVal]--] = array[i];
            }
            return sortedArray;
        }
        public static char[] FillWithRandShorts(char[] arr)
        {
            Random rnd = new Random();
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = (char)rnd.Next(-100, 10);
            }
            return arr;
        }
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;
            int lenght = 10000;
            char[] arr3 = new char[lenght];
          
            FillWithRandShorts(arr3);
            Stopwatch stopWatch3 = new Stopwatch();
            stopWatch3.Start();
            arr3 = countingSort(arr3);
            Show(arr3);
            stopWatch3.Stop();
            TimeSpan ts = stopWatch3.Elapsed;
            string elapsedTime3 = String.Format($"{ts.Minutes:00}:{ts.Seconds:00}:{ts.Milliseconds:00}");
            Console.WriteLine("Кiлькiсть елементiв масиву: " + lenght);
            Console.WriteLine("Час сортування методом пiдрахунка: " + elapsedTime3);
        }
      
     
    }
}

