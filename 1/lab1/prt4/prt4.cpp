﻿#include <stdio.h>
#include <conio.h>
#include <windows.h>
#include <math.h>
#include <ctype.h>
#include <time.h>
#include <ctime>
#include <malloc.h>
#include <iostream>
#include <string.h>
#define _CRT_SECURE_NO_WARNINGS

union data1
{
    float a;
    struct number1 {
        unsigned short x1 : 1;
        unsigned short x2 : 1;
        unsigned short x3 : 1;
        unsigned short x4 : 1;
        unsigned short x5 : 1;
        unsigned short x6 : 1;
        unsigned short x7 : 1;
        unsigned short x8 : 1;
        unsigned short x9 : 1;
        unsigned short x10 : 1;
        unsigned short x11 : 1;
        unsigned short x12 : 1;
        unsigned short x13 : 1;
        unsigned short x14 : 1;
        unsigned short x15 : 1;
        unsigned short x16 : 1;
        unsigned short x17 : 1;
        unsigned short x18 : 1;
        unsigned short x19 : 1;
        unsigned short x20 : 1;
        unsigned short x21 : 1;
        unsigned short x22 : 1;
        unsigned short x23 : 1;
        unsigned short x24 : 1;
        unsigned short x25 : 1;
        unsigned short x26 : 1;
        unsigned short x27 : 1;
        unsigned short x28 : 1;
        unsigned short x29 : 1;
        unsigned short x30 : 1;
        unsigned short x31 : 1;
        unsigned short x32 : 1;
    }n;
}x;
union data2
{
    float b;
    struct number2 {
        unsigned short x1 : 8;
        unsigned short x2 : 8;
        unsigned short x3 : 8;
        unsigned short x4 : 8;
    }d;
}c;
int main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    srand(time(0));
    float n;
    int a = sizeof(data1);
    int y = sizeof(data2);
    printf(" Введіть число:"); scanf_s("%f", &n);
    x.a = n;
    c.b = n;
    printf(" Значення побітово:");
    printf(" %f: %d%d%d%d %d%d%d%d %d%d%d%d %d%d%d%d %d%d%d%d %d%d%d%d %d%d%d%d %d%d%d%d\n", n, x.n.x1, x.n.x2, x.n.x3, x.n.x4, x.n.x5, x.n.x6, x.n.x7, x.n.x8, x.n.x9, x.n.x10, x.n.x11, x.n.x12, x.n.x13, x.n.x14, x.n.x15, x.n.x16, x.n.x17, x.n.x18, x.n.x19, x.n.x20, x.n.x21, x.n.x22, x.n.x23, x.n.x24, x.n.x25, x.n.x26, x.n.x27, x.n.x28, x.n.x29, x.n.x30, x.n.x31, x.n.x32);
    printf(" Значення побайтово:");
    printf(" %f: %d%d%d%d\n", n, c.d.x1, c.d.x2, c.d.x3, c.d.x4);
    printf(" Знак: %d\n", x.n.x32);
    printf(" Мантиса - %d%d%d%d %d%d%d%d %d%d%d%d %d%d%d%d %d%d%d%d %d%d%d\n", x.n.x23, x.n.x22, x.n.x21, x.n.x20, x.n.x19, x.n.x18, x.n.x17, x.n.x16, x.n.x15, x.n.x14, x.n.x13, x.n.x12, x.n.x11, x.n.x10, x.n.x9, x.n.x8, x.n.x7, x.n.x6, x.n.x5, x.n.x4, x.n.x3, x.n.x2, x.n.x1);
    printf(" data1 займає: %d\n", a);
    printf(" data2 займає: %d\n", y);
    return 0;
}
