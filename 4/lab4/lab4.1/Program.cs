﻿using System;

namespace Lab4_2_
{
    public class Node<N>
    {
        public N Data;
        public Node<N> Previous;
        public Node<N> Next;
        public Node(N Data)
        {
            this.Data = Data;
        }
    }
    public class DoubleLinkedList<N>
    {
        Node<N> head;
        Node<N> tail;
        public int count { get; private set; }

        public void Neww(N data)
        {
            Node<N> node = new Node<N>(data);

            if (head == null)
                head = node;
            else
            {
                tail.Next = node;
                node.Previous = tail;
            }
            tail = node;
            count++;
        }
        public void First(N data)
        {
            Node<N> node = new Node<N>(data);
            Node<N> temp = head;
            node.Next = temp;
            head = node;

            if (count == 0)
                tail = head;
            else
                temp.Previous = node;
            count++;
        }
        public bool Remove(N data)
        {
            Node<N> current = head;

            while (current != null)
            {
                if (current.Data.Equals(data))
                {
                    break;
                }
                current = current.Next;
            }
            if (current != null)
            {
                if (current.Next != null)
                {
                    current.Next.Previous = current.Previous;
                }
                else
                {
                    tail = current.Previous;
                }

                if (current.Previous != null)
                {
                    current.Previous.Next = current.Next;
                }
                else
                {
                    head = current.Next;
                }
                count--;
                return true;
            }
            return false;
        }
        public void Delete()
        {
            head = null;
            tail = null;
            count = 0;
        }
        public void Print()
        {
            Node<N> current = head;

            for (int i = 1; i <= count; i++)
            {
                Console.WriteLine($"{i}. {current.Data}");
                current = current.Next;
            }
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            DoubleLinkedList<int> list = new DoubleLinkedList<int>();
            list.Neww(1);
            list.Neww(3);
            list.Neww(6);
            list.Neww(12);
            list.Neww(14);
            list.Neww(7);
            list.Neww(17);
            list.First(99);
            list.Neww(15);
            list.Neww(18);
            list.Remove(7);
            list.Print();
            list.Delete();
            list.Print();
        }
    }
}
