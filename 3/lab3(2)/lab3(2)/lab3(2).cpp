﻿#include <iostream>
#include<windows.h>
#include <stdio.h> 
#include <time.h> 
using namespace std;

long double f(int a)
{
    if (a < 0)
        return 0;
    if (a == 0)
        return 1;
    else
        return a * f(a - 1);
}

int main()
{
    int a;
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    cout << "Input number: ";
    cin >> a;
    clock_t start = clock();
    if (a >= 0 && a <= 20) {
        cout << a << "! = " << f(a) << endl << endl;
    }
    else {
        printf("Error\n");
        return 0;
    }
    clock_t end = clock();
    double seconds = (double)(end - start) / CLOCKS_PER_SEC;
    printf("Time: %f секунди\n", seconds);
    return 0;
}
