﻿using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Diagnostics;
using System;
namespace lab5
{
    public class Node<N>
    {
        public N Data { get; set; }
        public Node<N> Previous { get; set; }
        public Node<N> Next { get; set; }
        public Node(N data)
        {
            Data = data;
        }
    }
    public class LinkedList1<N> : IEnumerable<N>
    {
        Node<N> head;
        Node<N> tail;
        int count;
        public int Count { get { return count; } }
        public bool IsEmpty { get { return count == 0; } }
        public void Add(N data)
        {
            Node<N> Node2 = new Node<N>(data);
            if (head == null)
                head = Node2;
            else


            {
                tail.Next = Node2;
                Node2.Previous = tail;
            }
            tail = Node2;
            count++;
        }
        public void Add(Node<N> Node2)
        {
            if (head == null)
                head = Node2;
            else
            {
                tail.Next = Node2;
                Node2.Previous = tail;
            }
            tail = Node2;
            count++;
        }

        public void AddFirst(N data)
        {
            Node<N> Node2 = new Node<N>(data);
            Node<N> temp = head;
            Node2.Next = temp;

            head = Node2;
            if (count == 0)
                tail = head;
            else
                temp.Previous = Node2;


            count++;
        }
        public void Remove(Node<N> data)
        {
            Node<N> curr = head;
            while (curr != null)
            {
                if (curr == data)
                    break;
                curr = curr.Next;


            }
            if (curr != null)
            {

                if (curr.Next != null)
                    curr.Next.Previous = curr.Previous;
                else
                    tail = curr.Previous;
                if (curr.Previous != null)
                    curr.Previous.Next = curr.Next;
                else
                    head = curr.Next;
                count--;
            }
        }




        public Node<N> GetTail()
        {
            return tail;
        }
        public Node<N> GetHead()
        {
            return head;
        }
        public void SetHead(Node<N> Node2)
        {
            Node<N> temp = head;
            Node2.Next = temp;
            head = Node2;
            temp.Previous = Node2;
        }

        public void Clear()
        {
            head = null;
            tail = null;
            count = 0;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)this).GetEnumerator();
        }


        IEnumerator<N> IEnumerable<N>.GetEnumerator()
        {
            Node<N> current = head;
            while (current != null)
            {
                yield return current.Data;
                current = current.Next;
            }
        }
    }



    public class Node2
    {
        public int V;
        public Node2 next;
        public Node2(int v)
        {
            this.V = v;
        }
    }


    public class LinkedList2
    {
        public Node2 head;
        public Node2 sorted;
        public int Count { get; private set; }
        public void Push(int val)
        {
            Node2 newnode = new Node2(val);
            newnode.next = head;
            head = newnode;
        }

        public void LiInSort(Node2 newHead)
        {
            sorted = null;
            Node2 current = newHead;
            while (current != null)
            {
                Node2 next = current.next;
                SortedInsert(current);
                current = next;
            }
            head = sorted;
        }

        public void SortedInsert(Node2 newNode)
        {
            if (sorted == null || sorted.V >= newNode.V)
            {
                newNode.next = sorted;
                sorted = newNode;
            }
            else
            {
                Node2 current = sorted;

                while (current.next != null &&
                        current.next.V < newNode.V)
                {
                    current = current.next;
                }

                newNode.next = current.next;
                current.next = newNode;
            }
        }
        public void Clear()
        {
            head = null;
            Count = 0;
        }

    }
    class Program
    {
        static void SelSort(Node<int> head)
        {
            Node<int> temp = head;

            while (temp != null)
            {
                Node<int> min = temp;
                Node<int> after = temp.Next;
                while (after != null)
                {
                    if (min.Data > after.Data)
                        min = after;

                    after = after.Next;

                }
                int x = temp.Data;
                temp.Data = min.Data;
                min.Data = x;
                temp = temp.Next;
            }
        }
        static void ArInSort(int[] array)
        {
            for (int i = 1; i < array.Length; i++)
            {
                int j;
                int tmp = array[i];

                for (j = i - 1; j >= 0; j--)
                {
                    if (array[j] < tmp)
                        break;

                    array[j + 1] = array[j];
                }
                array[j + 1] = tmp;
            }
        }
        static void Main(string[] args)
        {

            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;
            int temp = int.Parse(Console.ReadLine());
            Stopwatch st;

            Random rand = new Random();
            Console.WriteLine("Сортування вибором:");
            LinkedList1<int> List = new LinkedList1<int>();
            Node<int> head;

            for (int i = 0; i < temp; i++)
            {
                int newNode = rand.Next();
                List.Add(newNode);
            }
            head = List.GetHead();
            
            st = Stopwatch.StartNew();
            SelSort(head);
            st.Stop();
            
            Console.WriteLine($"Час сортування списку iз {temp} елементами: {st.Elapsed.TotalMilliseconds}");
            List.Clear();
            Console.WriteLine("Сортування масиву вставками:");
            int[] arr = new int[temp];
            
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = rand.Next();
            }
            st = Stopwatch.StartNew();
            ArInSort(arr);
            st.Stop();
            
            Console.WriteLine($"Час сортування масиву iз {temp} елементами: {st.Elapsed.TotalMilliseconds}");
            Console.WriteLine("Сортування списку вставками:");
            
            LinkedList2 list = new LinkedList2();
            
            for (int i = 0; i < temp; i++)
            {
                int newNode = rand.Next();
                list.Push(newNode);
            }
            st = Stopwatch.StartNew();
            
            list.LiInSort(list.head);
            st.Stop();
            
            Console.WriteLine($"Час сортування списку iз {temp} елементами: {st.Elapsed.TotalMilliseconds}");
            
            list.Clear();
        }
    }
}
