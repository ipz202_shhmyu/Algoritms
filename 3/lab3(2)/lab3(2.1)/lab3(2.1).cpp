﻿#include <iostream>
#include<windows.h>
#include <stdio.h> 
#include <time.h> 
using namespace std;

int main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    int m, b = 10, a = 1, fl;
    int tmp;
    printf("input length: "); scanf_s("%d", &m);
    int* arr = new int[m];
    for (int i = 0; i < m; i++) {
        arr[i] = a + rand() % (b - a + 1);
        printf("%d ", arr[i]);
    }
    if (m < 0 || m>1024) {
        printf("\nError\n");
        return 0;
    }
    printf("\n\n\nSorted\n");
    clock_t start = clock();
    do
    {
        fl = 0;
        for (int i = m; 0 < i; i--) {
            if (arr[i - 1] < arr[i]) {
                tmp = arr[i];
                arr[i] = arr[i - 1];
                arr[i - 1] = tmp;
                fl = 1;
            }
        }
    } while (fl);
    clock_t end = clock();
    double seconds = (double)(end - start) / CLOCKS_PER_SEC;
    for (int i = 0; i < m; i++) {
        printf("%d ", arr[i]);
    }
    printf("\n\nTime: %f sec\n\n", seconds);
    return 0;
}
