﻿#include <stdio.h>
#include <conio.h>
#include <windows.h>
#include <math.h>
#include <ctype.h>
#include <time.h>
#include <ctime>
#include <malloc.h>
#include <iostream>
#include <string.h>

#define _CRT_SECURE_NO_WARNINGS

struct date {
    unsigned short MonthDay : 6;
    unsigned short Month : 5;
    unsigned short Year : 8;
    unsigned short Hours : 6;
    unsigned short Minutes : 7;
    unsigned short Seconds : 7;
};
struct sdate {
    unsigned short nWeekDay : 3;
    unsigned short nMonthDay : 6;
    unsigned short nMonth : 5;
    unsigned short nYear : 8;
};
int main() {
    SetConsoleOutputCP(1251);
    SetConsoleCP(1251);
    struct date t;
    struct sdate m;
    unsigned short temp;
    printf("Day:"); scanf_s("%hu", &temp);
    t.MonthDay = temp;
    printf("Month:"); scanf_s("%hu", &temp);
    t.Month = temp;
    printf("Year:"); scanf_s("%hu", &temp);
    t.Year = temp;
    printf("Hours:"); scanf_s("%hu", &temp);
    t.Hours = temp;
    printf("Minutes:"); scanf_s("%hu", &temp);
    t.Minutes = temp;
    printf("Seconds:"); scanf_s("%hu", &temp);
    t.Seconds = temp;
    printf("%d.%d.%d\n", t.MonthDay, t.Month, t.Year);
    printf("%d:%d:%d\n", t.Hours, t.Minutes, t.Seconds);
    printf("Займає = %d \n", sizeof(date));
    printf("nWeekDay:"); scanf_s("%hu", &temp);
    m.nWeekDay = temp;
    printf("nMonthDay:"); scanf_s("%hu", &temp);
    m.nMonthDay = temp;
    printf("nMonth:"); scanf_s("%hu", &temp);
    m.nMonth = temp;
    printf("nYear:"); scanf_s("%hu", &temp);
    m.nYear = temp;
    printf("%d.%d.%d.%d\n", m.nWeekDay, m.nMonthDay, m.nMonth, m.nYear);
    int n = sizeof(sdate);
    printf("Займає = %d \n", n);
    return 0;
}
