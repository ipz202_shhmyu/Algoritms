﻿#include <iostream>
#include <time.h>
#include <Windows.h>
#define _CRT_SECURE_NO_WARNINGS

int main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    signed char a, b, c;
    a = 5;
    b = 127;
    c = a + b;
    printf("1) c = 5 + 127 = %d\n", c);
    a = 2;
    b = 3;
    c = a - b;
    printf("2) c = 2 - 3 = %d\n", c);
    a = -120;
    b = 34;
    c = a - b;
    printf("3) c = -120 - 34 = %d\n", c);
    unsigned char x;
    x = (unsigned char)-5;
    printf("4) c = (unsigned char) -5 = %d\n", x);
    c = 56 & 38;
    printf("5) c = 56 & 38 = %d\n", c);
    c = 56 | 38;
    printf("6) c = 56 | 38 = %d\n", c);
    return 0;
}
