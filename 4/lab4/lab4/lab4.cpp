﻿#include <iostream>
#include <iterator>
#include <windows.h>
#include <cstdlib>
#include <ctime>
#include <list>    
using namespace std;
int main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    list<int> List1;
    srand(time(NULL));
    for (int i = 0; i < 10; i++) {
        List1.push_back(rand() % 10);
    }
    printf("List N1:\n");
    copy(List1.begin(), List1.end(), ostream_iterator<int>(cout, " "));
    printf("\n");
    List1.sort();
    printf("Sorted list:\n");
    copy(List1.begin(), List1.end(), ostream_iterator<int>(cout, " "));
    printf("\n");
    List1.unique();
    printf("List without repeated elements:\n");
    copy(List1.begin(), List1.end(), ostream_iterator<int>(cout, " "));
    printf("\n");
    list<int> List2;
    for (int i = 0; i < 10; i++) {
        List2.push_back(rand() % 10);
    }
    printf("List N2:\n");
    copy(List2.begin(), List2.end(), ostream_iterator<int>(cout, " "));
    printf("\n");
    List2.push_front(10);
    List2.pop_back();
    return 0;
}
